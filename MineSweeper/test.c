#include"minesweeping.h"

int main() {
	int input = 0;
	int** data = NULL;
	char** map = NULL;
	int ret = 0;
	do {
		game_head();
		printf("输入1开始游戏>>");
		scanf("%d", &input);
		switch (input) {
		case PLAY:
			init(&data, &map);
			SetMine(&data);
			DispMap(map);
			int x = 0;
			int y = 0;
			
			while (1) {
				if (is_win() == 1) {
					printf("你赢了\n");
					break;
				}
				printf("请输入要查看的坐标>>");
				scanf("%d%d", &x, &y);
				ret = play_game(data, x, y);
				if (ret == 9) {
					printf("很遗憾，踩雷了\n");
					break;
				}
				else {
					change_map(&map, data, x, y);
				}
				DispMap(map);
			}
			break;
		case EXIT:
			printf("游戏结束\n");
			free(data);
			free(map);
			break;
		default:
			break;
		}
	} while (input != 0);
	return 0;
}