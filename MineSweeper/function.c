#include"minesweeping.h"

// 打印游戏头
void game_head() {
	printf("######################\n");
	printf("#        1.PLAY      #\n");
	printf("#        0.EXIT      #\n");
	printf("######################\n");
}

// 初始化数组
void init(int*** data, char*** map) {
	int i = 0;
	printf("==================\n");
	printf("      1.EASY      \n");
	printf("      2.MEZZO      \n");
	printf("      3.DIFF      \n");
	printf("==================\n");
	printf("请选择游戏难度>>");
	scanf("%d", &in);
	switch (in) {
	case EASY:
		*data = (int**)malloc(sizeof(int*) * (ES+2));
		*map = (char**)malloc(sizeof(char*) * (ES + 2));
		for (i = 0; i < ES + 2; i++) {
			(*data)[i] = (int*)malloc(sizeof(int) * (ES+2));
			(*map)[i] = (int*)malloc(sizeof(int) * (ES + 2));
		}
		for (i = 0; i < ES + 2; i++) {
			int j = 0;
			for (j = 0; j < ES + 2; j++) {
				(*data)[i][j] = 0;
				(*map)[i][j] = '*';
			}
		}
		break;
	case MEZZO:
		*data = (int**)malloc(sizeof(int*) * (MD + 2));
		*map = (char**)malloc(sizeof(char*) * (MD + 2));
		for (i = 0; i < MD + 2; i++) {
			(*data)[i] = (int*)malloc(sizeof(int) * (MD + 2));
			(*map)[i] = (int*)malloc(sizeof(int) * (MD + 2));
		}
		for (i = 0; i < MD + 2; i++) {
			int j = 0;
			for (j = 0; j < MD + 2; j++) {
				(*data)[i][j] = 0;
				(*map)[i][j] = '*';
			}
		}
		break;
	case DIFF:
		*data = (int**)malloc(sizeof(int*) * (MD + 2) );
		*map = (char**)malloc(sizeof(char*) * (MD + 2));
		for (i = 0; i < MD + 2; i++) {
			(*data)[i] = (int*)malloc(sizeof(int) * (HI + 2));
			(*map)[i] = (int*)malloc(sizeof(int) * (HI + 2));
		}
		for (i = 0; i < MD + 2; i++) {
			int j = 0;
			for (j = 0; j < HI + 2; j++) {
				(*data)[i][j] = 0;
				(*map)[i][j] = '*';
			}
		}
		break;
	default:
		break;
	}
}

// 打印地图

void DispMap(char** map) {
	int i = 0;
	int j = 0;
	system("CLS");
	switch (in) {
	case EASY:
		for (i = 1; i < ES + 1; i++) {
			if (i == 1) {
				printf(" |%d", i);
			}
			else
			{
				printf(" %d", i);
			}

		}
		printf("\n");
		for (i = 1; i < ES+1; i++) {
			printf("%d|", i);
			for (j = 1; j < ES+1; j++) {
				printf("%c ", map[i][j]);
			}
			printf("\n");
		}
		break;
	case MEZZO:
		for (i = 1; i < MD + 1; i++) {
			if (i == 1) {
				printf("  |%-2d", i);
			}
			else
			{
				printf(" %-2d", i);
			}

		}
		printf("\n");
		for (i = 1; i < MD+1; i++) {
			if (i < 10)
				printf("%d |", i);
			else
				printf("%d|", i);
			for (j = 1; j < MD+1; j++) {
				printf("%c  ", map[i][j]);
			}
			printf("\n");
		}
		break;
	case DIFF:
		for (i = 1; i < HI + 1; i++) {
			if (i == 1) {
				printf("  |%-2d", i);
			}
			else
			{
				printf(" %-2d", i);
			}

		}
		printf("\n");
		for (i = 1; i < MD+1; i++) {
			if (i < 10)
				printf("%d |", i);
			else
				printf("%d|", i);
			for (j = 1; j < HI+1; j++) {
				printf("%c  ", map[i][j]);
			}
			printf("\n");
		}
		break;
	}
	
}

// 放置地雷
void SetMine(int*** data) {
	int i = 0;
	int x = 0;
	int y = 0;
	srand((unsigned int)time(NULL));
	switch (in) {
	case EASY:
		i = 0;
		while (i < 10) {
			x = rand() % ES + 1;
			y = rand() % ES + 1;
			if ((*data)[x][y] == 0) {
				(*data)[x][y] = 1;
				i++;
			}
		}
		break;
	case MEZZO:
		i = 0;
		while (i < 30) {
			x = rand() % MD + 1;
			y = rand() % MD + 1;
			if ((*data)[x][y] == 0) {
				(*data)[x][y] = 1;
				i++;
			}
		}
		break;
	case DIFF:
		i = 0;
		while (i < 54) {
			x = rand() % MD + 1;
			y = rand() % HI + 1;
			if ((*data)[x][y] == 0) {
				(*data)[x][y] = 1;
				i++;
			}
		}
		break;
	}
}


// 玩游戏
int play_game(int** data, int x, int y) {
	int count = 0;
	if (data[x][y] == 1) {
		return 9;
	}
	else {
		if (data[x][y - 1] == 1) count++;
		if (data[x - 1][y - 1] == 1) count++;
		if (data[x - 1][y] == 1) count++;
		if (data[x - 1][y + 1] == 1) count++;
		if (data[x][y + 1] == 1) count++;
		if (data[x + 1][y + 1] == 1) count++;
		if (data[x + 1][y] == 1) count++;
		if (data[x + 1][y - 1] == 1) count++;
	}
	return count;
}

// 更改地图
void change_map(char*** map, int** data, int x, int y) {
	int ret = 0;
	switch (in) {
	case EASY:
		if ((x <= ES && x >0) && (y <= ES && y >0)) {
			ret = play_game(data, x, y);
			if (ret != 0) {
				(*map)[x][y] = ret + 48;
				win++;
			}
			else {
				(*map)[x][y] = ' ';
				win++;
				if ((*map)[x][y - 1] == '*')
					change_map(map, data, x, y - 1);
				if ((*map)[x][y + 1] == '*')
					change_map(map, data, x, y + 1);
				if ((*map)[x - 1][y] == '*')
					change_map(map, data, x - 1, y);
				if ((*map)[x + 1][y] == '*')
					change_map(map, data, x + 1, y);
			}
		}
		break;
	case MEZZO:
		if ((x <= MD && x > 0) && (y <= MD && y > 0)) {
			ret = play_game(data, x, y);
			if (ret != 0) {
				(*map)[x][y] = ret + 48;
				win++;
			}
			else {
				(*map)[x][y] = ' ';
				win++;
				if ((*map)[x][y - 1] == '*')
					change_map(map, data, x, y - 1);
				if ((*map)[x][y + 1] == '*')
					change_map(map, data, x, y + 1);
				if ((*map)[x - 1][y] == '*')
					change_map(map, data, x - 1, y);
				if ((*map)[x + 1][y] == '*')
					change_map(map, data, x + 1, y);
			}
		}
		break;
	case DIFF:
		if ((x <= MD && x > 0) && (y <= HI && y > 0)) {
			ret = play_game(data, x, y);
			if (ret != 0) {
				(*map)[x][y] = ret + 48;
				win++;
			}
			else {
				(*map)[x][y] = ' ';
				win++;
				if ((*map)[x][y - 1] == '*')
					change_map(map, data, x, y - 1);
				if ((*map)[x][y + 1] == '*')
					change_map(map, data, x, y + 1);
				if ((*map)[x - 1][y] == '*')
					change_map(map, data, x - 1, y);
				if ((*map)[x + 1][y] == '*')
					change_map(map, data, x + 1, y);
			}
		}
		break;
	}
}


int is_win() {
	switch (in) {
	case EASY:
		if (win == ES*ES-10)
			return 1;
		break;
	case MEZZO:
		if (win == MD*MD-30)
			return 1;
		break;
	case DIFF:
		if (win == HI*HI-54);
			return 1;
		break;
	}
	return 0;
}