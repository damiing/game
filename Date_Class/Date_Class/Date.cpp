#include"Date.h"
// 构造函数
Date::Date(int year, int month, int day) {
	if (year < 1900) {
		year = 1900;
	}
	if (month < 1 || month > 12) {
		month = 1;
	}
	if (day < 1 || day > GetMonthDay(year, month)) {
		day = 1;
	}
	_year = year;
	_month = month;
	_day = day;
}

// 拷贝构造
Date::Date(const Date& d) {
	this->_year = d._year;
	this->_month = d._month;
	this->_day = d._day;
}

// 析构函数
Date::~Date() {
	_year = 0;
	_month = 0;
	_day = 0;
}

// 获取某年某月的天数
int Date::GetMonthDay(int year, int month) {
	int monthDay[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int i = 0;
	// 判断是否闰年
	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
		if (month == 2) {
			return monthDay[month] + 1;
		}
		else {
			return monthDay[month];
		}
	}
	else {
		return monthDay[month];
	}
}

// 运算符重载

// 重载 == 运算符
bool Date::operator==(Date d){
	// ==符号，是只要年月日都相等，就相等
	return this->_year == d._year && this->_month == d._month && this->_day == d._day;
}

// 重载 + 运算符
Date Date::operator+(int day) {
	// 加法需要使用传值返回
	Date ret = *this;
	if (day < 0) {
		day *= -1;
		ret -= day;
		return ret;
	}
	ret._day += day;
	while (ret._day > ret.GetMonthDay(ret._year, ret._month)) {
		if (ret._day > GetMonthDay(ret._year, ret._month)) {
			ret._day = ret._day - GetMonthDay(ret._year, ret._month);
			++(ret._month);
			if (ret._month > 12) {
				++(ret._year);
				ret._month = 1;
			}
		}
		else {
			ret._day += day;
		}
	}
	return ret;
}

// 重载 += 运算符
Date& Date::operator+=(int day) {
	
	if (day < 0) {
		day *= -1;
		*this -= day;
		return *this;
	}
	_day += day;
	while (_day > GetMonthDay(_year, _month)) {
		_day = _day - GetMonthDay(_year, _month);
		++_month;
		if (_month > 12) {
			++_year;
			_month = 1;
		}
	}
	return *this;
}

// 重载 - 运算符
Date Date::operator-(int day) {

	Date ret = *this;
	if (day < 0) {
		day *= -1;
		ret += day;
		return ret;
	}
	ret._day -= day;
	while (ret._day < 1) {
		--(ret._month);
		if (ret._month < 1) {
			--(ret._year);
			ret._month = 12;
		}
		ret._day += GetMonthDay(ret._year, ret._month);
	}
	return ret;
}

// 重载 -= 运算符
Date& Date::operator-=(int day) {
	if (day < 0) {
		day *= -1;
		*this += day;
		return *this;
	}
	_day -= day;
	while (_day < 1) {
		--_month;
		if (_month < 1) {
			--_year;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

// 重载 > 运算符
bool Date::operator>(const Date d) {
	if (_year > d._year) {
		return true;
	}
	else if (_year == d._year) {
		if (_month > d._month) {
			return true;
		}
		else if (_month == d._month) {
			if (_day > d._day) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

// 重载 >= 运算符
bool Date::operator>=(const Date d) {
	return *this > d || *this == d;
}

// 重载 != 运算符
bool Date::operator!=(const Date d) {
	return !(*this == d);
}

// 重载  <  运算符
bool Date::operator<(const Date d) {
	return !(*this >= d);
}
// 重载 <= 运算符
bool Date::operator<=(const Date d) {
	return !(*this > d);
}

// 重载 = 运算符
Date& Date::operator=(const Date d) {
	if (this != &d) {
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	return *this;
}

// 重载 前置++ 运算符
Date& Date::operator++() {
	*this += 1;
	return *this;
}

// 重载 后置++ 运算符
Date Date::operator++(int) {
	Date ret = *this;
	*this += 1;
	return ret;
}

// 重载 前置-- 运算符
Date& Date::operator--() {
	*this += 1;
	return *this;
}

// 重载 后置-- 运算符
Date Date::operator--(int) {
	Date ret = *this;
	*this += 1;
	return ret;
}

// 日期 - 日期得到间隔天数
int Date::operator-(Date d) {
	Date min = *this > d ? d:*this;
	Date max = *this > d ? *this : d;
	int count = 0;
	while (min < max) {
		++min;
		++count;
	}
	return count;
}

// 重载 流插入<<
ostream& operator<<(ostream& out, Date d) {
	out << d._year << d._month << d._day;
	return out;
}
// 重载 流提取>>
istream& operator>>(istream& in, Date d) {
	in >> d._year >> d._month >> d._day;
	return in;
}