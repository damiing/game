#include"Date.h"


void test1() {
	Date d1(2000, 10, 1);
	Date d2(1999, 2, 1);
	Date d3(1890, 0, 32);
	Date d4(2000, 2, 29);
}

void test2() {
	Date d1;
	Date d2(2000, 12,29);

	cout << (d1 == d2) << endl;

	Date d3 = d2 + (-1000) + 100 + 20;
	d2 += 4000;

}

void test3() {

	Date d1;
	Date d2(2000, 1, 20);

	Date d3 = d2 - 1000;
	d2 -= -4000;

}

void test4() {

	Date d1;
	Date d2(2000, 1, 1);
	Date d3(2022, 1, 20);
	Date d4(2000, 1, 1);
	cout << (d4 <= d2) << endl;
}

void test5() {
	Date d1;
	Date d2(2000, 1, 1);
	cout << (d2 - d1) << endl;
	cout << d1 << d2;
}
int main() {
	test5();
	return 0;
}