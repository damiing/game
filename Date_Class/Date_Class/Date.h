#pragma once
#include<iostream>
using namespace std;
class Date {
private: 
	int _year;
	int _month;
	int _day;
public:
	// 缺省行构造函数
	Date(int year = 1, int month = 1, int day = 1);
	// 实现拷贝构造(const通吃)
	Date(const Date& d);
	// 析构函数
	~Date();
	// 重载 == 运算符
	bool operator==(const Date d);
	// 重载 > 运算符
	bool operator>(const Date d);
	// 重载 >= 运算符
	bool operator>=(const Date d);
	// 重载 != 运算符
	bool operator!=(const Date d);
	// 重载  <  运算符
	bool operator<(const Date d);
	// 重载 <= 运算符
	bool operator<=(const Date d);
	// 重载 + 运算符
	Date operator+(int day);
	// 重载 += 运算符
	Date& operator+=(int day);
	// 重载 - 运算符
	Date operator-(int day);
	// 重载 -= 运算符
	Date& operator-=(int day);
	// 重载 = 运算符
	Date& operator=(const Date d);
	// 重载 前置++ 运算符
	Date& operator++();
	// 重载 后置++ 运算符
	Date operator++(int);
	// 重载 前置-- 运算符
	Date& operator--();
	// 重载 后置-- 运算符
	Date operator--(int);
	// 日期 - 日期得到间隔天数
	int operator-(Date d);
private:
	// 获取某年某月的天数
	int GetMonthDay(int year, int month);
	// 重载 流插入<<
	friend ostream& operator<<(ostream& out, Date d);
	// 重载 流提取>>
	friend istream& operator>>(istream& in, Date d);

};

// 重载 流插入<<
istream& operator<<(istream& out, Date d);
// 重载 流提取>>
ostream& operator>>(ostream& in, Date d);