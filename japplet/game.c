#include "game.h"

// 打印游戏头
void game_head() {
	printf("####################\n");
	printf("#      1.PLAY      #\n");
	printf("#      0.EXIT      #\n");
	printf("####################\n");
}

// 初始化游戏地图
void InitMap(char arr[ROW][COL], int row, int col) {
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			arr[i][j] = ' ';
		}
	}
}

// 打印游戏地图
void map() {
	//system("CLS");
	int row = 0;
	int col = 0;
	for (row = 0; row < ROW; row++) {
		for (col = 0; col < COL; col++) {
			if (col < COL - 1) {
				printf(" %c |", arr[row][col]);
			}
			else {
				printf(" %c \n", arr[row][col]);
			}
		}
		if (row < ROW - 1) {
			for (col = 0; col < COL; col++) {
				printf("----");
			}
		}
		printf("\n");
	}
}

// 判断游戏是否结束
int is_end() {
	int i = 0;
	int flag = 0;
	if ((arr[0][0] == arr[1][0] && arr[1][0] == arr[2][0] && arr[0][0] != ' ') ||
		(arr[0][1] == arr[1][1] && arr[1][1] == arr[2][1] && arr[0][1] != ' ') ||
		(arr[0][2] == arr[1][2] && arr[1][2] == arr[2][2] && arr[0][2] != ' ') ||
		(arr[0][0] == arr[0][1] && arr[0][1] == arr[0][2] && arr[0][0] != ' ') ||
		(arr[1][0] == arr[1][1] && arr[1][1] == arr[1][2] && arr[1][0] != ' ') ||
		(arr[2][0] == arr[2][1] && arr[2][1] == arr[2][2] && arr[2][0] != ' ') ||
		(arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2] && arr[0][0] != ' ') ||
		(arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0] && arr[0][2] != ' ')) {
		return 1;
	}
	char* tmp = arr;
	for (i = 0; i < ROW * COL; i++) {
		if (*tmp == ' ') {
			tmp = tmp + 1;
			flag = 1;
			break;
		}
		tmp = tmp + 1;
	}
	if (flag == 0) {
		return 0;
	}
	else {
		return -1;
	}

}

// 玩游戏
void play_game() {
	int x = 0;
	int y = 0;
	while (1) {
		printf("请输入坐标>");
		scanf("%d%d", &x, &y);
		if ((x >= 1 && x <= ROW) && (y >= 1 && y <= COL)) {
			if (arr[x - 1][y - 1] == ' ') {
				arr[x - 1][y - 1] = 'X';
				break;
			}
			else {
				printf("这个地方已经下过了，请重新输入\n");
			}
		}
		else {
			printf("输入不合法，请重新输入\n");
		}
	}
}

void cmp_game() {
	int cmp_x = 0;
	int cmp_y = 0;
	while (1) {
		if (is_end() != -1) {
			break;
		}
		cmp_x = rand() * 10 % 3;
		cmp_y = rand() * 10 % 3;
		if (arr[cmp_x][cmp_y] == ' ') {
			arr[cmp_x][cmp_y] = 'O';
			break;
		}
	}
}

// 判断谁获胜
char is_win() {
	if ((arr[0][0] == arr[1][0] && arr[1][0] == arr[2][0] && arr[0][0] != ' ')||
		(arr[0][0] == arr[0][1] && arr[0][1] == arr[0][2] && arr[0][0] != ' ')) {
		return arr[0][0];
	}
	if ((arr[0][1] == arr[1][1] && arr[1][1] == arr[2][1] && arr[0][1] != ' ') ||
		(arr[1][0] == arr[1][1] && arr[1][1] == arr[1][2] && arr[1][0] != ' ') ||
		(arr[2][0] == arr[1][1] && arr[1][1] == arr[0][2] && arr[0][2] != ' ')||
		(arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2] && arr[0][0] != ' ')) {
		return arr[1][1];
	}
	if ((arr[0][2] == arr[1][2] && arr[1][2] == arr[2][2] && arr[0][2] != ' ') ||
		(arr[2][0] == arr[2][1] && arr[2][1] == arr[2][2] && arr[2][0] != ' ')) {
		return arr[2][2];
	}
	return ' ';
}