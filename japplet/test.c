#include "game.h"


int main() {

	int input = 0;
	int cmp_x = 0;
	int cmp_y = 0;
	srand((unsigned int)time(NULL));

	do {
		game_head();
		printf("输入1开始游戏>>");
		scanf("%d", &input);
		InitMap(arr, ROW, COL);
		switch (input) {
		case PLAY:
			map();
			while (is_end() == -1) {

				play_game();
				map();
				cmp_game();
				map();
			}
			if (is_win() == 'X') {
				printf("玩家获胜\n");
			}
			else if (is_win() == 'O') {
				printf("电脑获胜\n");
			}
			else {
				printf("平局\n");
			}
			
			break;
		case EXIT:
			printf("游戏结束!\n");
			break;
		default:
			printf("输入不合法，请重新输入\n");
		}
	} while (input != 0);
	return 0;
}