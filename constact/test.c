#include"constact.h"
int main() {
	int input = 0;
	int index;
	char str[20] = { 0 };
	constacts* con = NULL;
	initconstacts(&con);
	printf("%d\n", sizeof(constacts));
	do {
 		menu();
		printf("请选择要进行的操作>>");
		scanf("%d", &input);
		switch (input) {
		case ADD:
			add(con);
			break;
		case DEL:
			del(con);
			break;
		case SELECT:
			printf("请输入要查找的人的名字>>");
			scanf("%s", str);
			index = select(con,str);
			if (-1 == index) {
				printf("查无此人\n");
			}
			else {
				show(con, index, 1);
			}
			break;
		case MODIFY:
			modify(con);
			break;
		case SHOW:
			show(con,0,con->size);
			break;
		case SORT:
			sort(con,0, con->size-1);
			break;
		case EXIT:
			free(con);
			con = NULL;
			break;
		default:
			printf("输入有误，请重新输入\n");
			break;
		}
	} while (input);
	return 0;
}