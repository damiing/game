#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

#define MAX_NAME 20
#define MAX_PHONE 12
#define MAX_ADDRESS 20
#define MAX_PEO 1000
// 枚举可以进行的操作
enum option {
	EXIT,
	ADD,
	DEL,
	SELECT,
	MODIFY,
	SHOW,
	SORT
};

// 修改联系人的操作选项

enum mdoption {
	EX,
	NAME,
	AGE,
	PHONE,
	ADDRESS
};

// 定义联系人的格式
typedef struct people {
	char name[MAX_NAME];
	unsigned int age;
	char phone[MAX_PHONE];
	char address[MAX_ADDRESS];
}people;

// 定义一个存放联系人信息的空间
#if 0
typedef struct constacts {
	people* data[MAX_PEO];
	unsigned int size;
}constacts;
#endif

// 定义一个可边长的通讯录

typedef struct constacts {
	people** data;
	int capacity;  // 定义容量，不够的时候扩容
	unsigned int size;

}constacts;


// 打印菜单
void menu();
void menu2();

// 初始化通讯录
void initconstacts(constacts** con);

// 添加联系人
void add(constacts* con);

// 删除联系人
void del(constacts* con);

// 查找联系人
int select(constacts* con, char str[]);

// 修改联系人
void modify(constacts* con);

// 打印通讯录
void show(constacts* con, int index, int num);

// 给通讯录排序
void sort(constacts* con, int begin, int end);