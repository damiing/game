#include"constact.h"

// 打印菜单
void menu() {
	printf("################################\n");
	printf("##   1.add           2.del    ##\n");
	printf("##   3.selcet        4.modify ##\n");
	printf("##   5.show          6.sort   ##\n");
	printf("##           0.sort           ##\n");
	printf("################################\n");
}

void menu2() {
	printf("#############################\n");
	printf("### 1.name      2.age     ###\n");
	printf("### 3.phone     4.address ###\n");
	printf("#########  0.exit   #########\n");
	printf("#############################\n");
}

//初始化通讯录
#if 0
void initconstacts(constacts** con) {
	constacts* tmp = (constacts*)malloc(sizeof(constacts));
	memset(tmp, 0, sizeof(constacts));
	*con = tmp;
}
#endif

void initconstacts(constacts** con) {
	constacts* tmp = (constacts*)malloc(sizeof(constacts));
	people** tmp_p = (people**)malloc(sizeof(people*) * 3);
	memset(tmp, 0, sizeof(constacts));
	*con = tmp;
	(*con)->data = tmp_p;
	(*con)->capacity = 3;
	(*con)->size = 0;
}


// 添加联系人
#if 0
void add(constacts* con) {
	if (con->size == MAX_PEO) {
		printf("通讯录已满无法在增加新联系人\n");
		return;
	}
	assert(con != NULL);
	people* pe = (people*)malloc(sizeof(people));
	printf("请输入姓名>>");
	scanf("%s", &(pe->name));
	printf("请输入年龄>>");
	scanf("%d", &(pe->age));
	printf("请输入电话号码>>");
	scanf("%s", &(pe->phone));
	printf("请输入家庭住址>>");
	scanf("%s", &(pe->address));
	con->data[con->size] = pe;
	con->size++;
}
#endif

void add(constacts* con) {
	if (con->size == con->capacity) {
		people** tmp = (people**)realloc(con->data, sizeof(people*) * (con->capacity * 2));
		if (tmp == NULL) {
			printf("扩容失败\n");
			return;
		}
		con->data = tmp;
		con->capacity *= 2;
	}
	assert(con != NULL);
	people* pe = (people*)malloc(sizeof(people));
	printf("请输入姓名>>");
	scanf("%s", &(pe->name));
	printf("请输入年龄>>");
	scanf("%d", &(pe->age));
	printf("请输入电话号码>>");
	scanf("%s", &(pe->phone));
	printf("请输入家庭住址>>");
	scanf("%s", &(pe->address));
	con->data[con->size] = pe;
	con->size++;
}
// 删除联系人
void del(constacts* con) {
	if (con->size == 0) {
		printf("通讯录为空，无需进行此操作\n");
		return;
	}
	assert(con != NULL);
	int index = 0;
	printf("请输入要删除人的名字>>");
	char str[20] = { 0 };
	scanf("%s", str);
	index = select(con, str);
	if (-1 == index) {
		printf("查无此人\n");
	}
	else {
		int i = 0;
		for (i = index; i < con->size; i++) {
			con->data[i] = con->data[i + 1];
		}
		con->size--;
	}
}

// 查找联系人
int select(constacts* con, char str[]) {
	assert(con != NULL);
	int i = 0;
	while (i < con->size) {
		if (strcmp(con->data[i]->name, str) == 0) {
			return i;
		}
		else {
			i++;
		}
	}
	return -1;
}

// 修改联系人
void modify(constacts* con) {
	assert(con != NULL);
	int input = 0;
	char name[20] = { 0 };
	printf("请输入要修改的联系人>>");
	scanf("%s", &name);
	int index = select(con, name);
	if (index == -1) {
		printf("没有该联系人，请添加\n");
		add(con);
	}else{
		do {
			menu2();
			printf("请选择要修改的内容>>");
			scanf("%d", &input);
			switch (input) {
			case NAME:
				printf("新名字>>");
				scanf("%s", con->data[index]->name);
				break;
			case AGE:
				printf("新名字>>");
				scanf("%s", con->data[index]->name);
				break;
			case PHONE:
				printf("新名字>>");
				scanf("%s", con->data[index]->name);
				break;
			case ADDRESS:
				printf("新名字>>");
				scanf("%s", con->data[index]->name);
				break;
			case EX:
				break;
			default:
				printf("选择错误\n");
				break;
			}
		} while (input);
	}
}

// 打印通讯录
void show(constacts* con,int index, int num) {
	assert(con != NULL);
	printf("姓名\t\t年龄\t\t电话号码\t\t家庭住址\n");
	int i = index;
	while(num--) {
		printf("%s\t\t%d\t\t%s\t\t%s\n", con->data[i]->name, con->data[i]->age, con->data[i]->phone, con->data[i]->address);
		i++;
	}
}

// 给通讯录排序
void sort(constacts* con, int begin, int end) {
	
	if (end <= begin) {
		return;
	}

	people key = *(con->data[0]);
	int pivot = 0;

	while (begin < end) {
		while (begin < end && strcmp(con->data[end]->name, key.name) > 0) {
			end--;
		}
		*(con->data[pivot]) = *(con->data[end]);
		pivot = end;

		while (begin < end && strcmp(con->data[begin]->name, key.name) < 0) {
			begin++;
		}
		*(con->data[pivot]) = *(con->data[begin]);
		pivot = begin;
	}
	*(con->data[pivot]) = key;
	sort(con, begin, pivot - 1);
	sort(con, pivot + 1, end);

}